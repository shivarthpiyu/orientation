# Git Summary

## Introduction to Git:
- Git is a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency. 
- Git is easy to learn and has a tiny footprint with lightning fast performance.
- It outclasses SCM tools like Subversion, CVS, Perforce, and ClearCase with features like cheap local branching, convenient staging areas, and multiple workflows.

![Git logo](https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Git-logo.svg/1024px-Git-logo.svg.png)

## Common Terminologies in Git:
These are most commonly used terms in git.

- ### Repository:
    This is Often referred to as a 'Repo'. This is essentially where the source code (files and folders) reside that we use Git to track.

- ### Commit:
    Commit is used to Save the changes in our repository or files.

- ### Branch:
    Simply put, a branch in Git is a lightweight movable pointer to one of these commits. The default branch name in Git is master. As we start making commits, we are given a master branch that points to the last commit made.

- ### Push: 
    Pushing is essentially syncing the local repository with remote (accessible over the internet) branches.

- ### Merge: 
    This is pretty much self-explanatory. In a very basic sense, it is integrating two branches together.

- ### Clone: 
    Again cloning is pretty much what it sounds like. It takes the entire remote (accessible over the internet) repository and/or one or more branches and makes it available locally.

- ### Fork:
    Forking allows us to duplicate an existing repo under our username.

## Installing Git:
Git probably didn’t come installed on your computer, so we have to get it there. Luckily, installing git is super easy, whether you’re on Linux, Mac, or Windows.

For Linux, open the terminal and type:
```sh
$ sudo apt-get install git 
```
It only works on Ubuntu. If not, there is a list of all the [Linux package installation commands](https://git-scm.com/download/linux) for whichever other distro you’re on.

For windows users, Git can be installed by [downloading installer](https://git-scm.com/download/win) and run it.

Similarly for Mac users, [download the installer](https://git-scm.com/download/mac) and run it.

For checking installation is succesfull or not,
- Open git terminal.
- Enter:
```sh
    $ git --version
```
It should give you installed git version.
- Configure your Git username and email using the following commands, replacing user-name and user-emailID with your own. These details will be associated with any commits that you create:

```sh
$ git config --global <user-name>
$ git config --global <user-emailID>
```
## About Git:
#### Open Source
Git is released under the GNU General Public License version 2.0, which is an open source license. The Git project chose to use GPLv2 to guarantee your freedom to share and change free software---to make sure the software is free for all its users.

#### Branching and Merging
- The Git feature that really makes it stand apart from nearly every other SCM out there is its branching model.
- Git allows and encourages you to have multiple local branches that can be entirely independent of each other. The creation, merging, and deletion of those lines of development takes seconds.

#### Small and Fast
 Git is fast. With Git, nearly all operations are performed locally, giving it a huge speed advantage on centralized systems that constantly have to communicate with a server somewhere. Git was built to work on the Linux kernel, meaning that it has had to effectively handle large repositories from day one. Git is written in C, reducing the overhead of runtimes associated with higher-level languages. Speed and performance has been a primary design goal of the Git from the start.

#### Data Assurance
The data model that Git uses ensures the cryptographic integrity of every bit of your project. Every file and commit is checksummed and retrieved by its checksum when checked back out. It's impossible to get anything out of Git other than the exact bits you put in.

## Basic Git Workflow:
The basic Git work flow goes something like this:

![Work Flow](https://miro.medium.com/max/1000/1*zw0bLFWkaAP2QPfhxkoDEA.png)

## Git commands:

1. cloning a repo:
```sh
$ git clone <link-to-repository>
```
2.	Creating a new branch:
```sh
$ git checkout master
$ git checkout -b <your-branch-name>
```

For modifying files in your working tree,

You selectively stage just those changes you want to be part of your next commit, which adds only those changes to the staging area.

```sh
$ git add .         # To add untracked files ( . adds all files) 
```

1.	You do a commit, which takes the files as they are in the staging area and stores that snapshot permanently to your Local Git Repository.

```sh
$ git commit -sv   # Description about the commit
```

2.	You do a push, which takes the files as they are in the Local Git Repository and stores that snapshot permanently to your Remote Git Repository.

```sh
$ git push origin <branch-name>      # push changes into repository
```

Some other git commands are:

![](https://rubygarage.s3.amazonaws.com/uploads/article_image/file/599/git-cheatsheet-5.jpg)

When moved to remote repository, we can get access over the internet. The files in local repositories are only available in our device (as they stored in our local storage drives).


# Three stages in Git:

- **Modified/update** : This means you have changes the contents of the file but not commited it to
the local repository
- **Staged** : This mean that you have moves the changed file to the index to be commited in the local
repository
- **Commit**: This means that you have stored your changed data safelyt in your local repository.

![image1](https://support.nesi.org.nz/hc/article_attachments/360004194235/Git_Diagram.svg)






